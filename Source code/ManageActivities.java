import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ManageActivities {


private List<MonitoredData> activities;
    public ManageActivities(List<MonitoredData> activities)
    {
        this.activities=activities;
    }

    public List<MonitoredData> getActivities()
    {
        return this.activities;
    }

//task2
//Se calculeaza si pentru start date si pentru end date si se face maximul de zile
    //daca s-ar face pe o singura varianta(end sau start)
    //ar putea sa existe cazuri speciale in care o activitate incepe intr-o zi, si se termina in alta(daca e ultima, va disparea din count) de exemplu.
    public Map<String,Long> task2()
    {
        Map<String,Long> hartaFinala;

        Map<String,Long> countingDaysStart;

        countingDaysStart=activities.stream()
                .collect(Collectors.groupingBy(MonitoredData::getDayMonthYearStart,Collectors.counting()));

        Map<String,Long> countingDaysEnd;
        countingDaysEnd=activities.stream()
                .collect(Collectors.groupingBy(MonitoredData::getDayMonthYearEnd,Collectors.counting()));


        if(countingDaysStart.size()>countingDaysEnd.size())
        {
            hartaFinala=countingDaysStart;
        }
        else{
            hartaFinala=countingDaysEnd;
        }

        return hartaFinala;
    }

    //task3
    public Map<String,Long> task3()
    {
        Map<String,Long> hartaActivitati=new HashMap<>();

        hartaActivitati=activities.stream()
                .collect(Collectors.groupingBy(MonitoredData::getActivity,Collectors.counting()));

        return hartaActivitati;
    }


    public Map<Integer,Map<String,Long>> task4()
    {
        Map<Integer,Map<String,Long>> harta=new HashMap<>();

        harta=activities.stream().
                collect(Collectors.groupingBy(MonitoredData::getDay,Collectors.groupingBy(MonitoredData::getActivity,Collectors.counting())));

        return harta;
    }


    public Map<String, Long> task5()
    {
        Map<String, Long> harta=new HashMap<>();

         harta=activities.stream()
                 .collect(Collectors.groupingBy(MonitoredData::getActivity,Collectors.summingLong(MonitoredData::computeSeconds)));

        return harta;
    }


    public List<String> task6()
    {
        List<String> lista=new LinkedList<>();

        lista=activities.stream()
                .filter(p->p.computeSeconds()<300)
                .map(MonitoredData::getActivity).distinct().collect(Collectors.toList());


        return lista;
    }

}
