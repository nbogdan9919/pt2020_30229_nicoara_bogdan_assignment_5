import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class MonitoredData {

private String start_time;
private String end_time;
private String activity;


    public MonitoredData(String start_time, String end_time, String activity) {
        this.start_time = start_time;
        this.end_time = end_time;
        this.activity = activity;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getActivity() {

        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public LocalDateTime stringToDate(String s)
    {
        DateTimeFormatter formatter=DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime date=LocalDateTime.parse(s,formatter);

        return date;

    }

    public String getDayMonthYearStart()
    {
        String s="";

        LocalDateTime date=this.stringToDate(this.getStart_time());

        s=String.valueOf(date.getDayOfMonth())+date.getMonth()+date.getYear();

        return s;

    }

    public String getDayMonthYearEnd()
    {
        String s="";
        LocalDateTime date=this.stringToDate(this.getEnd_time());

        s=String.valueOf(date.getDayOfMonth())+" "+date.getMonth()+" "+date.getYear();

        return s;
    }

    public int getDay()
    {
        LocalDateTime date=this.stringToDate(this.getEnd_time());

        return (date.getYear()*100+date.getMonthValue())*100+date.getDayOfMonth();

    }

    public long computeSeconds()
    {
        long totalSeconds;

        LocalDateTime dateEnd=this.stringToDate(this.getEnd_time());
        LocalDateTime dateStart=this.stringToDate(this.getStart_time());

        Duration dur=Duration.between(dateStart,dateEnd);

        totalSeconds=dur.getSeconds();
        return totalSeconds;


    }



    public String toString()
    {
        String s="";
        s=s+"Start time:( "+this.start_time+" ) End time: ( "+this.end_time+" ) Activity:( "+this.activity+" ) ";

        return s;
    }
}
