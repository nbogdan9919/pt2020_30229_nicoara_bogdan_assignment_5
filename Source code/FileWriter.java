import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class FileWriter {

    ManageActivities manager;

public FileWriter(List<MonitoredData> activities)
{
    manager=new ManageActivities(activities);

}

public File createFileIesire(String out) {
    File iesire = null;

    try {
        iesire = new File(out);
        if (!iesire.exists()) {
            iesire.createNewFile();
        }

    } catch (IOException e) {
        e.printStackTrace();
    }

    return iesire;
}

    public void printTask1()
    {
        List<MonitoredData> activities=manager.getActivities();
        File iesire=createFileIesire("Task_1.txt");
        PrintWriter pw;

            try {
                pw=new PrintWriter(iesire);

                activities.forEach(activity->pw.println(activity.toString()));

                pw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    public void printTask2()
    {

        File iesire=createFileIesire("Task_2.txt");
        PrintWriter pw;
        String s;
        int days;
        Map<String,Long> hartaFinala;

        try {
            pw=new PrintWriter(iesire);
            hartaFinala=manager.task2();
            days=hartaFinala.size();


            pw.println("zile diferite: "+hartaFinala.size()+"\n");
            pw.println("zilele sunt:\n");

            hartaFinala.forEach((Key,Value)->pw.println(Key));

            pw.println("\nin total ("+days+") zile diferite");

            pw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    public void printTask3()
    {
        Map<String,Long> hartaActivitati=manager.task3();
        File iesire=createFileIesire("Task_3.txt");
        PrintWriter pw;

        try {
            pw=new PrintWriter(iesire);

            hartaActivitati.forEach((Key, Value)->pw.println(Key+" "+Value));

            pw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String intFormat(int number)
    {
        String s="";
        s=String.valueOf(number/10000)+"_"
                +String.valueOf((number%10000)/100)+"_"
                +String.valueOf(number%100);

        return s;
    }

    public void printTask4()
    {
        Map<Integer,Map<String,Long>> harta=manager.task4();

        File iesire=createFileIesire("Task_4.txt");
        PrintWriter pw;

        try {
            pw=new PrintWriter(iesire);

            harta.forEach((Key,Value)->pw.println(intFormat(Key) +" "+ Value));

            pw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public String secondsToHH_MM_SS(Long seconds)
    {
        String s="";
        int hours=(int) (seconds/3600);
        int minutes = (int) ((seconds - hours * 3600) / 60);
        int second = (int)( (seconds - hours * 3600) - minutes * 60);
        s=s+hours+" hours, "+minutes+" minutes, "+second+" seconds";

        return s;
    }

    public void printTask5()
    {
        Map<String,Long> harta=manager.task5();

        File iesire=createFileIesire("Task_5.txt");
        PrintWriter pw;

        try {
            pw=new PrintWriter(iesire);

            harta.forEach((Key,Value)->pw.println(Key+" "+secondsToHH_MM_SS(Value)+"\n"));

            pw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void printTask6()
    {
        List<String> lista=manager.task6();
        File iesire=createFileIesire("Task_6.txt");
        PrintWriter pw;

        try {
            pw=new PrintWriter(iesire);

           lista.forEach(pw::println);

            pw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
