import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DataReader {


    public DataReader()
    {

    }

    //Task1
    public List<MonitoredData> task1(String intrare)
    {
        List<MonitoredData> listaMonitoredData= new LinkedList<>();

        File file=new File(intrare);
        try(Stream<String> stream = Files.lines(Paths.get(intrare)))
        {
            listaMonitoredData=stream.map(line->line.split("\\s{2,}|\\t"))
                    .map(a->new MonitoredData(a[0],a[1],a[2]))
                    .collect(Collectors.toList());

        }catch(Exception e)
        {

        }
        return listaMonitoredData;
    }

}
